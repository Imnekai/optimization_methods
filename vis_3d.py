from matplotlib import cm
import matplotlib
from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import time
from IPython import display
import config as cfg




def animate(function, methods = None, steps=100, fps=1e12,
						start=(-4,0.5), mode='section', save=False):
	if methods is None:
		methods = cfg.default
	else:
		if type(methods)!=dict:
			aux = methods
			methods = {}
			for method in aux:
				methods[method]={}
		for method in methods.keys():
			for prop in cfg.default[method].keys():
				if prop not in methods[method]:
					methods[method][prop]=cfg.default[method][prop]

	tf.reset_default_graph()

	plt.ion()
	fig = plt.figure(figsize=cfg.figsize, dpi=cfg.dpi)
	ax = fig.add_subplot(111, projection='3d')
	plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
	params = {'legend.fontsize': 5,
				'legend.handlelength': 5}
	plt.rcParams.update(params)
	plt.axis('off')
	x = tf.placeholder(dtype=tf.float32, shape=[None, 1])
	y = tf.placeholder(dtype=tf.float32, shape=[None, 1])
	z = function(x,y)


	x_val = y_val = np.arange(cfg.bounds_3d[0], cfg.bounds_3d[1], 
														0.1, dtype=np.float32)
	x_val_mesh, y_val_mesh = np.meshgrid(x_val, y_val)
	x_val_mesh_flat = x_val_mesh.reshape([-1, 1])
	y_val_mesh_flat = y_val_mesh.reshape([-1, 1])
	with tf.Session() as sess:
		z_val_mesh_flat = sess.run(z, feed_dict={x: x_val_mesh_flat, 
																							y:y_val_mesh_flat})
	z_val_mesh = z_val_mesh_flat.reshape(x_val_mesh.shape)
	levels = np.arange(0, 30, 0.5)
	if mode=='section':
		ax.contour(x_val_mesh, y_val_mesh, z_val_mesh, levels, 
								alpha=.7, linewidths=0.4)
	elif mode=='wire':
		ax.plot_wireframe(x_val_mesh, y_val_mesh, z_val_mesh, alpha=.5, 
										linewidths=0.4, antialiased=True)
	elif mode=='surface':
		ax.plot_surface(x_val_mesh, y_val_mesh, z_val_mesh, alpha=.4, cmap=cm.Spectral)
	plt.draw()


	x_var = {}
	y_var = {}
	for method in methods.keys():
		x_var[method]=tf.get_variable('var_x_'+method, [1], 
								initializer= tf.initializers.constant(start[0]))
		y_var[method]=tf.get_variable('var_y_'+method, [1], 
								initializer= tf.initializers.constant(start[1]))

	
	cost = {}
	for method in methods.keys():
		cost[method]=function(x_var[method],y_var[method])

	ops = {}
	for method in methods.keys():
		if method=='adadelta':
			ops[method]=tf.train.AdadeltaOptimizer(methods[method]['lr'])\
				.minimize(cost[method])
		if method=='adam':
			ops[method]=tf.train.AdamOptimizer(methods[method]['lr'],
				epsilon=1e-12, beta1=methods[method]['alpha'],
				beta2=methods[method]['beta']).minimize(cost[method])
		if method=='nadam':
			ops[method]=tf.contrib.opt.NadamOptimizer(methods[method]['lr'],
				epsilon=1e-12, beta1=methods[method]['alpha'],
				beta2=methods[method]['beta']).minimize(cost[method])
		if method=='adagrad':
			ops[method]=tf.train.AdagradOptimizer(methods[method]['lr'],
				initial_accumulator_value=1e-12).minimize(cost[method])
		if method=='gd':
			ops[method]=tf.train.GradientDescentOptimizer(methods[method]['lr'])\
				.minimize(cost[method])
		if method=='rmsprop':
			ops[method]=tf.train.RMSPropOptimizer(methods[method]['lr'],
				epsilon=1e-12,decay=methods[method]['beta']).minimize(cost[method])
		if method=='momentum':
			ops[method]=tf.train.MomentumOptimizer(methods[method]['lr'],
				methods[method]['alpha'], use_nesterov=False).minimize(cost[method])
		if method=='nesterov':
			ops[method]=tf.train.MomentumOptimizer(methods[method]['lr'],
				methods[method]['alpha'], use_nesterov=True).minimize(cost[method])
		
	xlm = ax.get_xlim3d()
	ylm = ax.get_ylim3d()
	zlm = ax.get_zlim3d()
	ax.set_xlim3d(xlm[0] * 0.5, xlm[1] * 0.5)
	ax.set_ylim3d(ylm[0] * 0.5, ylm[1] * 0.5)
	ax.set_zlim3d(zlm[0] * 0.5, zlm[1] * 0.5)
	azm = ax.azim
	ele = ax.elev+10
	ax.view_init(elev=ele, azim=azm)

	config = tf.ConfigProto()
	config.gpu_options.allow_growth = True
	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())

	
		last_x, last_y, last_z = [], [], []
		plot_cache = [None for _ in range(len(methods))]

	
		for iter in range(steps):
			for i, method in enumerate(methods.keys()):
			
				if iter==0:
					x_val, y_val, z_val = sess.run([x_var[method], y_var[method], 
																				cost[method]])
					last_x.append(x_val)
					last_y.append(y_val)
					last_z.append(z_val)

				
				sess.run(ops[method])
				x_val, y_val, z_val = sess.run([x_var[method], y_var[method], 
																				cost[method]])
				x_val = x_val[0]
				y_val = y_val[0]
				z_val = z_val[0]
				
			
				if x_val < cfg.bounds_3d[0] or x_val>cfg.bounds_3d[1] or \
						y_val < cfg.bounds_3d[0] or y_val>cfg.bounds_3d[1]:
					continue
				if plot_cache[i]:
					plot_cache[i].remove()
				plot_cache[i] = ax.scatter(x_val, y_val, z_val, s=30, depthshade=True,
					label=method, color=methods[method]['color'])

			
				ax.plot([last_x[i], x_val], [last_y[i], y_val], [last_z[i], z_val],
								linewidth=1, color=methods[method]['color'])
				last_x[i]=x_val
				last_y[i]=y_val
				last_z[i]=z_val
			

			if iter == 0:
			
			
				plt.legend(plot_cache, zip(methods.keys(),
																[methods[m]['lr'] for m in methods.keys()]))
			if save:
				plt.savefig('img/' + str(iter) + '.png')
		
		
			plt.pause(1/fps)
			fig.canvas.draw()

if __name__=='__main__':
	animate(lambda x,y: 0.1*tf.square(x)+9.95*tf.square(y), 
	 				methods=['gd'],mode='surface', steps=80)