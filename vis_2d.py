from matplotlib import cm
import matplotlib
from mpl_toolkits.mplot3d import Axes3D

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import time
from IPython import display
import config as cfg





def animate(function, methods = None, steps=100, fps=30,start=-3,save=False,
						bounds=cfg.bounds_2d, plot_detail=0.1):
	if methods is None:
		methods = cfg.default
	else:
		if type(methods)!=dict:
			aux = methods
			methods = {}
			for method in aux:
				methods[method]={}
		for method in methods.keys():
			for prop in cfg.default[method].keys():
				if prop not in methods[method]:
					methods[method][prop]=cfg.default[method][prop]

	tf.reset_default_graph()
	
	plt.ion()
	fig = plt.figure(figsize=cfg.figsize, dpi=cfg.dpi)
	
	
	
	ax = fig.add_subplot(111)
	plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=0, hspace=0)
	params = {'legend.fontsize': 3,
				'legend.handlelength': 3}
	plt.rcParams.update(params)
	plt.axis('off')
	x = tf.placeholder(dtype=tf.float32, shape=[None])
	y = function(x)

	
	x_val = np.arange(bounds[0], bounds[1], plot_detail, dtype=np.float32)
	with tf.Session() as sess:
		y_val = sess.run(y, feed_dict={x: x_val})
	
	
	ax.plot(x_val, y_val, linewidth=1, color='black')
	plt.draw()
	

	x_var = {}
	for method in methods.keys():
		x_var[method]=tf.get_variable('var_'+method, [1], 
								initializer= tf.initializers.constant(start))

	
	cost = {}
	for method in methods.keys():
		cost[method]=function(x_var[method])

	ops = {}
	for method in methods.keys():
		if method=='adadelta':
			ops[method]=tf.train.AdadeltaOptimizer(methods[method]['lr'])\
				.minimize(cost[method])
		if method=='adam':
			ops[method]=tf.train.AdamOptimizer(methods[method]['lr'],
				epsilon=1e-12, beta1=methods[method]['alpha'],
				beta2=methods[method]['beta']).minimize(cost[method])
		if method=='nadam':
			ops[method]=tf.contrib.opt.NadamOptimizer(methods[method]['lr'],
				epsilon=1e-12, beta1=methods[method]['alpha'],
				beta2=methods[method]['beta']).minimize(cost[method])
		if method=='adagrad':
			ops[method]=tf.train.AdagradOptimizer(methods[method]['lr'],
				initial_accumulator_value=1e-12).minimize(cost[method])
		if method=='gd':
			ops[method]=tf.train.GradientDescentOptimizer(methods[method]['lr'])\
				.minimize(cost[method])
		if method=='rmsprop':
			ops[method]=tf.train.RMSPropOptimizer(methods[method]['lr'],
				epsilon=1e-12,decay=methods[method]['beta']).minimize(cost[method])
		if method=='momentum':
			ops[method]=tf.train.MomentumOptimizer(methods[method]['lr'],
				methods[method]['alpha'], use_nesterov=False).minimize(cost[method])
		if method=='nesterov':
			ops[method]=tf.train.MomentumOptimizer(methods[method]['lr'],
				methods[method]['alpha'], use_nesterov=True).minimize(cost[method])
		
	config = tf.ConfigProto()
	config.gpu_options.allow_growth = True
	with tf.Session(config=config) as sess:
		sess.run(tf.global_variables_initializer())

		
		last_x, last_y = [], []
		plot_cache = [None for _ in range(len(methods))]

		
		for iter in range(steps):
			for i, method in enumerate(methods.keys()):
				

				if iter==0:
					x_val, y_val= sess.run([x_var[method], cost[method]])
					last_x.append(x_val)
					last_y.append(y_val)
				sess.run(ops[method])
				x_val, y_val = sess.run([x_var[method], cost[method]])
				
				if(x_val < bounds[0] or x_val>bounds[1]):
					continue
				
				if plot_cache[i]:
					plot_cache[i].remove()
				plot_cache[i] = ax.scatter(x_val, y_val, 
					label=method, color=methods[method]['color'])

		
				ax.plot([last_x[i], x_val], [last_y[i], y_val],
									linewidth=0.5, color=methods[method]['color'])
				last_x[i]=x_val
				last_y[i]=y_val
			

			if iter == 0:
				
				
				plt.legend(plot_cache, zip(methods.keys(),
																[methods[m]['lr'] for m in methods.keys()]))

			
			
			if save:
				plt.savefig('img/' + str(iter) + '.png')
			plt.pause(1/fps)
			fig.canvas.draw()

if __name__=='__main__':
	animate(lambda x: tf.square(x), 
					methods=['nadam'], start=-0.2,bounds=[-0.2,0.2],
					plot_detail=0.01, steps=120)