figsize = (3.5, 2.2)

dpi = 300

default = {	
						'adagrad': {'lr':0.1, 'color': 'g'},
						'adam': {'lr':0.1, 'beta':0.9, 'alpha':0.9, 'color': 'r'},
						'nadam': {'lr':0.1, 'beta':0.9, 'alpha':0.9, 'color': 'b'},
						'gd': {'lr':0.1, 'color':'springgreen'},
						'momentum': {'lr':0.1, 'alpha':0.9, 'color':'gold'},
						'nesterov': {'lr':0.1, 'alpha':0.9, 'color':'magenta'},
						'rmsprop': {'lr':0.1, 'beta':0.9, 'color':'c'}}

bounds_2d=[-3.5,3.5]
bounds_3d=[-4,4]
