import matplotlib.pyplot as plt
from matplotlib import rc
rc('font',**{'size': 26})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)
import numpy as np



def var_seq(n):
	low_var = np.random.normal(0,0.2,n)
	high_var = np.random.normal(0,3,n)
	plt.subplots_adjust(wspace=0.8)
	plt.subplot(211)
	plt.title(r'$\nabla L(\theta_t^1)$')
	#plt.xlabel('t')
	plt.ylim(-10,10)
	plt.xticks([])
	plt.plot(low_var)
	plt.subplot(212)
	#plt.xlabel('t')
	plt.title(r'$\nabla L(\theta^2_t)$')
	plt.ylim(-10,10)
	plt.xticks([])
	plt.plot(high_var)
	plt.show()


if __name__=='__main__':
	var_seq(200)